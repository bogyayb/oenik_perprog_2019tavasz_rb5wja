﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Video.FFMPEG;
using System.Drawing;
using System.Diagnostics;

namespace PerProgBeadando
{
    class Program
    {
        static void Main(string[] args)
        {
            var imagesCount = 200;
            Bitmap image = new Bitmap("inputImage/inputImage.jpg");

            //SequentalProcess(image, imagesCount);
            ParallelProcess(image, imagesCount);

            Console.ReadKey();
        }

        //methods
        static void createVideo(int imagesCount, int width, int height, int FPS)
        {
            var videoWriter = new VideoFileWriter();

            videoWriter.Open("outputVideo/intro.avi", width, height, FPS, VideoCodec.MPEG4, 1000000);

            for (int imageFrame = 1; imageFrame < imagesCount; imageFrame++)
            {
                var imgPath = string.Format("frames/frame{0}.jpg", imageFrame);

                using (Bitmap image = Bitmap.FromFile(imgPath) as Bitmap)
                {
                    videoWriter.WriteVideoFrame(image);
                }
            }

            videoWriter.Close();
        }

        static void SequentalProcess(Bitmap image, int imagesCount)
        {
            Stopwatch stopper = new Stopwatch();
            stopper.Start();

            #region első csík
            int f = 0;

            List<Bitmap> newFrames = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        if (j > image.Height / 4)
                        {
                            newFrames[v].SetPixel(i, j, Color.Black);
                        }
                        else if (i > v * (image.Width / 50))
                        {
                            newFrames[v].SetPixel(i, j, Color.Black);
                        }

                    }
                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f++;
            }

            #endregion

            #region második csík
            f += 49;

            List<Bitmap> newFrames2 = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames2.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        //ha a saját sávja alatt vagyunk --> fekete pixel
                        if (j > (image.Height / 4) * 2)
                        {
                            newFrames2[v].SetPixel(i, j, Color.Black);
                        }

                        //ha a saját sávja felett vagyunk --> eredeti pixel
                        else if (j < (image.Height / 4) * 1) { }

                        //
                        else if (i < v * (image.Width / 50))
                        {
                            newFrames2[v].SetPixel(i, j, Color.Black);
                        }
                    }
                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames2[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f--;
            }

            #endregion

            #region harmadik csík
            f = 100;

            List<Bitmap> newFrames3 = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames3.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        //ha a saját sávja alatt vagyunk --> fekete pixel
                        if (j > (image.Height / 4) * 3)
                        {
                            newFrames3[v].SetPixel(i, j, Color.Black);
                        }

                        //ha a saját sávja felett vagyunk --> eredeti pixel
                        else if (j < (image.Height / 4) * 2) { }

                        //
                        else if (i > v * (image.Width / 50))
                        {
                            newFrames3[v].SetPixel(i, j, Color.Black);
                        }
                    }

                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames3[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f++;
            }

            #endregion

            #region negyedik csík
            f += 49;

            List<Bitmap> newFrames4 = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames4.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        //ha a saját sávja alatt vagyunk --> fekete pixel
                        if (j > (image.Height / 4) * 4)
                        {
                            newFrames4[v].SetPixel(i, j, Color.Black);
                        }

                        //ha a saját sávja felett vagyunk --> eredeti pixel
                        else if (j < (image.Height / 4) * 3) { }

                        //
                        else if (i < v * (image.Width / 50))
                        {
                            newFrames4[v].SetPixel(i, j, Color.Black);
                        }
                    }

                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames4[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f--;
            }

            #endregion

            //kimeneti videó készítése
            createVideo(imagesCount, image.Width, image.Height, 100);

            //idő kiírása
            stopper.Stop();

            CimKiiras("Eltelt idő: " + MillisecundumKonvertalasIdove(stopper.ElapsedMilliseconds), ConsoleColor.Green);

        }
        
        static void ParallelProcess(Bitmap image, int imagesCount)
        {
            Stopwatch stopper = new Stopwatch();
            stopper.Start();

            List<Task> lt = new List<Task>();

            object lockObject = new object();
            int lockedImgWidth;
            int lockedImgHeight;
            lock (lockObject)
            {
                lockedImgWidth = image.Width;
                lockedImgHeight = image.Height;
            }

            lt.Add(new Task(() => {
                ElsoCsik(lockedImgWidth, lockedImgHeight);
                Console.WriteLine("1");
            }, TaskCreationOptions.LongRunning));

            lt.Add(new Task(() => {
                MasodikCsik(lockedImgWidth, lockedImgHeight);
                Console.WriteLine("2");

            }, TaskCreationOptions.LongRunning));

            lt.Add(new Task(() => {
                HarmadikCsik(lockedImgWidth, lockedImgHeight);
                Console.WriteLine("3");

            }, TaskCreationOptions.LongRunning));

            lt.Add(new Task(() => {
                NegyedikCsik(lockedImgWidth, lockedImgHeight);
                Console.WriteLine("4");

            }, TaskCreationOptions.LongRunning));

            foreach (var t in lt)
            {
                t.Start();
            }


            Task.WhenAll(lt).ContinueWith(ts =>
            {           
                createVideo(imagesCount, image.Width, image.Height, 100);

                stopper.Stop();

                CimKiiras("Eltelt idő: " + MillisecundumKonvertalasIdove(stopper.ElapsedMilliseconds), ConsoleColor.Green);
            });
        }

        static void ElsoCsik(int lockedImgWidth, int lockedImgHeight)
        {
            #region első csík
            int f = 0;

            List<Bitmap> newFrames = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < lockedImgWidth; i++)
            {
                for (int j = 0; j < lockedImgHeight; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        if (j > lockedImgHeight / 4)
                        {
                            newFrames[v].SetPixel(i, j, Color.Black);
                        }
                        else if (i > v * (lockedImgWidth / 50))
                        {
                            newFrames[v].SetPixel(i, j, Color.Black);
                        }

                    }
                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f++;
            }

            #endregion
        }

        static void MasodikCsik(int lockedImgWidth, int lockedImgHeight)
        {
            #region második csík
            int f = 99;

            List<Bitmap> newFrames2 = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames2.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < lockedImgWidth; i++)
            {
                for (int j = 0; j < lockedImgHeight; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        //ha a saját sávja alatt vagyunk --> fekete pixel
                        if (j > (lockedImgHeight / 4) * 2)
                        {
                            newFrames2[v].SetPixel(i, j, Color.Black);
                        }

                        //ha a saját sávja felett vagyunk --> eredeti pixel
                        else if (j < (lockedImgHeight / 4) * 1) { }

                        //
                        else if (i < v * (lockedImgWidth / 50))
                        {
                            newFrames2[v].SetPixel(i, j, Color.Black);
                        }
                    }
                }

            }

            for (int i = 0; i < 50; i++)
            {
                newFrames2[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f--;
            }

            #endregion
        }

        static void HarmadikCsik(int lockedImgWidth, int lockedImgHeight)
        {

            #region harmadik csík
            int f = 100;

            List<Bitmap> newFrames3 = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames3.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < lockedImgWidth; i++)
            {
                for (int j = 0; j < lockedImgHeight; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        //ha a saját sávja alatt vagyunk --> fekete pixel
                        if (j > (lockedImgHeight / 4) * 3)
                        {
                            newFrames3[v].SetPixel(i, j, Color.Black);
                        }

                        //ha a saját sávja felett vagyunk --> eredeti pixel
                        else if (j < (lockedImgHeight / 4) * 2) { }

                        //
                        else if (i > v * (lockedImgWidth / 50))
                        {
                            newFrames3[v].SetPixel(i, j, Color.Black);
                        }
                    }
                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames3[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f++;
            }

            #endregion
        }

        static void NegyedikCsik(int lockedImgWidth, int lockedImgHeight)
        {
            #region negyedik csík
            int f = 199;

            List<Bitmap> newFrames4 = new List<Bitmap>();

            for (int i = 0; i < 50; i++)
            {
                newFrames4.Add(new Bitmap("inputImage/inputImage.jpg"));
            }

            for (int i = 0; i < lockedImgWidth; i++)
            {
                for (int j = 0; j < lockedImgHeight; j++)
                {
                    for (int v = 0; v < 50; v++)
                    {
                        //ha a saját sávja alatt vagyunk --> fekete pixel
                        if (j > (lockedImgHeight / 4) * 4)
                        {
                            newFrames4[v].SetPixel(i, j, Color.Black);
                        }

                        //ha a saját sávja felett vagyunk --> eredeti pixel
                        else if (j < (lockedImgHeight / 4) * 3) { }

                        //
                        else if (i < v * (lockedImgWidth / 50))
                        {
                            newFrames4[v].SetPixel(i, j, Color.Black);
                        }
                    }
                }
            }

            for (int i = 0; i < 50; i++)
            {
                newFrames4[i].Save($"frames/frame{f}.jpg");
                Console.WriteLine($"frame{f} - done");
                f--;
            }

            #endregion

        }


        //segédmetódusok
        private static string MillisecundumKonvertalasIdove(long millisecundum)
        {
            TimeSpan ido = TimeSpan.FromMilliseconds(millisecundum);
            string formazott = string.Format("{1:D2}:{2:D2}.{3:D3}", ido.Hours, ido.Minutes, ido.Seconds, ido.Milliseconds);
            return formazott;
        }

        
        private static void CimKiiras(string szoveg, ConsoleColor szin)
        {
            Console.Clear();
            Console.ForegroundColor = szin;

            for (int i = 0; i < szoveg.Length; i++)
            {
                Console.Write("-");
            }

            Console.WriteLine("");
            Console.WriteLine(szoveg);

            for (int i = 0; i < szoveg.Length; i++)
            {
                Console.Write("-");
            }

            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

    }
}
